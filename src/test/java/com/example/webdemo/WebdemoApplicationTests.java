package com.example.webdemo;

import org.hibernate.Session;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.PersistenceContext;

@SpringBootTest
class WebdemoApplicationTests {

	@PersistenceContext
	private Session session;
	@Autowired
	private restaurantDAO dao;

	@Test
	void contextLoads() {
		System.out.println(dao.getallfood());
	}

}
