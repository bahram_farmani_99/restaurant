package com.example.webdemo.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Restaurants {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private Double latitude;
    private Double longitude;
    private String image;
    private String distancewithme;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Food> foods = new ArrayList<Food>();

    @OneToMany(cascade = CascadeType.ALL)
    private List<Meal> meals = new ArrayList<Meal>();


    public List<Food> getFoods() {
        return foods;
    }

    public void setFoods(List<Food> foods) {
        this.foods = foods;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDistancewithme() {
        return distancewithme;
    }

    public void setDistancewithme(String distancewithme) {
        this.distancewithme = distancewithme;
    }
}
