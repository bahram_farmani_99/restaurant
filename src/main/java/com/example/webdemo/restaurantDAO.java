package com.example.webdemo;

import com.example.webdemo.Entity.Food;
import com.example.webdemo.Entity.Meal;
import com.example.webdemo.Entity.Restaurants;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class restaurantDAO {

    @PersistenceContext
    private Session session;

    @Transactional
    public List<Food> getallfood(){
        Query q = session.createQuery("from Food");
        List<Food> result = (List<Food>) ((org.hibernate.query.Query<?>) q).list();
        return result;
    }

    @Transactional(isolation = Isolation.DEFAULT)
    public List<Meal> getallmeal(){
        Query q = session.createQuery("from Meal ");
        return (List<Meal>) ((org.hibernate.query.Query<?>) q).list();
    }

    @Transactional(isolation = Isolation.DEFAULT)
    public List<Restaurants> getallrestaurant(){
        Query q = session.createQuery("from Restaurants ");
        return (List<Restaurants>) ((org.hibernate.query.Query<?>) q).list();
    }
}
