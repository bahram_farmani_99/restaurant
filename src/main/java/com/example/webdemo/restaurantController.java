package com.example.webdemo;

import com.example.webdemo.Entity.Food;
import com.example.webdemo.Entity.Meal;
import com.example.webdemo.Entity.Restaurants;
import org.hibernate.mapping.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.awt.*;
import java.util.List;

@RestController
public class restaurantController {
    @Autowired
    private restaurantDAO dao;

    @GetMapping("/food")
    public List<Food> getallfood() {
        List<Food> content = dao.getallfood();
        return content;
    }

    @GetMapping("/meal")
    public List<Meal> getallmeal() {
        List<Meal> content = dao.getallmeal();
        return content;
    }

    @GetMapping("/restaurant")
    public List<Restaurants> getallrestaurant() {
        List<Restaurants> content = dao.getallrestaurant();
        return content;
    }


}
